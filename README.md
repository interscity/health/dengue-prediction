## Introduction

This is dengue prediction model developed by Robson Aleixo during his master degree. The supervisor is Raphael Camargo and the co-supervisor is Fabio Kon. This project is part of InterSCity research group and had a partnership with the IEPS group.

The scope of this project is to predict dengue cases for each neighborhood in Rio de Janeiro, Brazil. Data were used: from January of 2011 to October of 2020, available on the NationalHealth  Notification  Information  System  (SINAN); Temperature and precipitation measurement from weatherstations  from  the  National  Weather  Institute  (INMET);demographic informa-tion  from  the  Brazilian  Institute  of  Geography  and  Statistics(IBGE);  and  the  number  of  public  health  facilities  andrelated  information  from  the  National  Register  of  HealthFacilities (CNES).

## Requirements

The code was developed in Python 3.6 and pip 20.0.2. 
The libraries required specification is in requirements.txt file

## How to run

To run the main analysis just execute main.py file.

## For more information

Visit https://interscity.org/health/ or contact us at interscity at ime dot usp dot br
